Pong = {
	
	Startup: {
		window: {width:'800',height:'600'},
		paddle: {top:175,left:50},
		court: {width: 800,height: 500},
		ball: {width: 10,height: 10,top: 0,left: 0}
	},
	
	
	/* Constructor for Pong */
	initialize: function(){
		Pong.Startup.window.width = $(document).width();
		Pong.Startup.window.height = $(document).height();
		Pong.Startup.court.width = (Pong.Startup.window.width - 50);
		Pong.Startup.court.height = (Pong.Startup.window.height - 50);
		this.initializeObjects();
	},
	
	go: function(){
		Pong.Ball.launch();
	},
	
	
	/* Objects of Pong */
	
	initializeObjects: function (){
		this.Court.initialize();
		this.Paddle.initialize();
		this.Ball.initialize();
	},
	
	Court: {
		
		initialize: function (){
			this.width = Pong.Startup.court.width;
			this.height = Pong.Startup.court.height;
			this.draw();
		}, 
		
		draw: function (){
			$('#main').append('<div id="court" class="court"></div>');
			$('#court')
			.css({
				'border':'2px solid white', 
				'background-color': 'black',
				'width':this.width,
				'height':this.height
			});
			
			$('#main #court').append('<div id="court_line"></div>');
			$('#court_line').css({
				'position':'relative',
				'left':((this.width/2)-10),
				'height':'100%',
				'width':'1px',
				'border-right':'10px dotted white',
				'float':'left'
			});
		}		
	},

	Paddle: {
		initialize: function(){
			this.width = 15;
			this.height = 100;
			this.draw();
			Pong.moveSomething('paddle_1', Pong.Startup.paddle.top, Pong.Startup.paddle.left);
			//making a reference to the function 
			followMouse = this.followMouse();
			setInterval('followMouse',1);
		},
		
		draw: function (){
			$('#main .court').append('<div id="paddle_1" class="paddle"></div>');
			$('.paddle')
			.css({
				'background-color':'white', 
				'width':this.width,
				'height':this.height,
				'float':'left'
			});
		}, 
		
		followMouse: function(){
			$('#main').mousemove(function(e){
				
				mouse_y_to = e.pageY-50;
				if (e.pageY > (Pong.Court.height - Pong.Paddle.height) ) {
					mouse_y_to = (Pong.Court.height-Pong.Paddle.height);
				}
				
				Pong.moveSomething('paddle_1', mouse_y_to, Pong.Startup.paddle.left);
			});
		}
		
	},
	
	Ball: {
		initialize: function(){
			this.width = Pong.Startup.ball.width,
			this.height = Pong.Startup.ball.height,
			this.draw();
			this.cur_ball_top = Pong.Startup.court.height/2;
			this.cur_ball_left = Pong.Startup.court.width/2;
			
			$('.ball').bind('stopped', function() {
				console.log('Object stopped');
			});
			
			Pong.moveSomething('ball', this.cur_ball_top , this.cur_ball_left );
		},
		
		draw: function (){
			$('#main #court').append('<div id="ball" class="ball"></div>');
			$('.ball').css({
				'background-color':'red', 
				'width':this.width,
				'height':this.height,
				'float':'left'
			});
		},
		
		//launch in a random direction
		launch: function(){
			ball_top_to = Math.floor((Math.random()*Pong.Startup.court.height)+1 );
			ball_left_to = Math.floor((Math.random()*Pong.Startup.court.width)+1);
			Pong.moveSomething('ball', ball_top_to, ball_left_to, 1000);
		},
		
		start: function(){
			setInterval(function(){
				//is ball moving?
				//move ball
			}, 1)
		},
		
		update: function(){
			//this.cur_ball_top = '';
			//this.cur_ball_left = '';
			//count how many px up till I hit a wall.
			//count how many px left till I hit a wall.
		}
		
	},
	
	Collision: {
		wallCollision: function(){},
		paddleCollision: function(){}
	},
	
	
	/* Methods */
	
	moveSomething: function (selector, top, left, speed){
		
		selector = $('#'+selector);
		
		$(selector).css({'position':'relative'})
		
		//if a speed has been set
		if (speed !== undefined){
			selector.animate({'top':top,'left':left}, speed, 'linear');
		}  else {
			selector.css({
				'top':top,
				'left':left
			});
		}
			
	}
	
	
	
		
	
}

